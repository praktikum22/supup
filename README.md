# SupUp

![Get Your Sup Up](./frontend/src/Images/header.png "Get Your Sup Up")

Project that focuses on finding the right places for you to SUP at. With the help of other users' ratings and opinions you can find the location that suits your needs the most. You can choose between different locations, types of water, popularity, distance and so much more. Get your Sup Up!

The working webpage can be found at http://194.152.16.191/

## Visuals
![Locations Map with filter](./promotion/Screenshoti/zemljevid.png "Locations Map with filter")
![Location Details](./promotion/Screenshoti/podrobno_lokacija.png "Location Details")

## Installation
### Building locally
To work locally with this project, you'll have to follow the steps below:

1. Clone the repo to your machine
```
git clone https://gitlab.com/praktikum22/supup.git
```
##### 2.1. Backend 
Install Dependencies in its' Own Terminal
```
npm install
```
or
```
yarn install
```
Run the App
```
npm start
```
##### 2.2. Frontend 
Install Dependencies in its' Own Terminal
```
npm install
```
or
```
yarn install
```
Run the App
```
npm start
```


3. Update configurations (optional) <br />
Additionally, while the majority of dependencies will be managed by a package manager like yarn or npm, there may be additional dependencies needed on your machine to run your code. In many cases, after some initial setup and if you are working primarily in one language or framework, projects can be up and running with a simple ```npm install``` and ```npm start```.


##### Connecting to the Database
4. Download [MongoDB Compass](https://downloads.mongodb.com/compass/mongodb-compass-1.31.3-win32-x64.msi)
4. On MongoDB click New Connection, copy the URI and replace it within the .env file. Then connect to the database.
5. Go to Database, click Create Database and name it "Test", name the collection "locationmessages".
6. Go to "locationmessages" collection and click Add Data. There add the locations file from the database folder.

##### Logging in with default user
The default username is ```supup.user@gmail.com``` and the passowrd is ```user123```

## Work in the future
While we are proud of what we have accomplished so far, in the future we would like to add for the user to get directions to the location from his current location and be able to export that in a given form (e.g. gpx...) <br />
We would also like to add recommended locations, based on user's favourite locations.


## Authors and acknowledgment
Univerza v Mariboru - Fakulteta za elektrotehniko, računalništvo in informatiko <br />
Authors: Borak, Fras, Kostantinovič <br />
Mentors: Tilen Hliš
