import React, { useEffect, useRef } from "react";
import "./App.css";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import { useDispatch } from "react-redux";
import { getLocation } from "./actions/locations";
import Home from "./components/Home/Home";
import About from "./components/Subpages/About";
import SignIn from "./components/Subpages/SignIn";
import ForgotPassword from "./components/Subpages/ForgotPassword";
import Register from "./components/Subpages/Register";
import Footer from "./components/Footer/Footer";
import AddLocation from "./components/AddLocation/AddLocation";
import Navbar from "./components/Navbar/Navbar";
import LocationsContainer from "./components/Locations/LocationsContainer";
import LocationDetails from "./components/LocationDetails/LocationDetails";
import NotFound from "./components/Subpages/NotFound";
import ProfileContainer from "./components/Subpages/ProfileContainer";

function App() {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getLocation());
  }, [dispatch]);

  return (
    <Router>
      <div className="App">
        <Navbar />
        <Routes>
          <Route exact path="/" element={<Home />} />
          <Route path="/about" element={<About />} />
          <Route path="/add-location" element={<AddLocation />} />
          <Route path="/sign-in" element={<SignIn />} />
          <Route path="/profile" element={<ProfileContainer />} />
          <Route path="/forgot-password" element={<ForgotPassword />} />
          <Route path="/register" element={<Register />} />
          <Route path="/locations" element={<LocationsContainer/>} />
          <Route path="/location/:id" element={<LocationDetails />} />
          <Route exact path="/*" element={<NotFound />} />
        </Routes>
        <br />
        <br />
        <br />
        <Footer />
      </div>
    </Router>
  );
}
export default App;
