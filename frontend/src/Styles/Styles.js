import { makeStyles } from "@material-ui/core";
import headerImage from "../Images/header.png";

const Styles = makeStyles((theme) => ({
  appBar: {
    backgroundColor: "#FFF",
    height: "70px",
  },
  text1: {
    width: "100%",
    textAlign: "center",
    borderBottom: "3px solid #ffffff",
    lineHeight: "0.1em",
    margin: "10px 0 20px",
    fontSize: "30px",
    color: "#ffffff",
    fontWeight: "600",
    fontFamily: "Garamond, serif",
  },

  hero: {
    backgroundImage: `url(${headerImage})`,
    height: "500px",
    backgroundPosition: "center",
    backgroundSize: "cover",
    position: "relative",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    [theme.breakpoints.down("sm")]: {
      height: 300,
      fontSize: "3em",
    },
  },
  link: {
    fontSize: "17px",
    color: "#041e3f",
    fontWeight: "600",
    textDecoration: "none",
    padding: "20px",
    "&:hover": {
      background: "#1f406b",
      color: "#ffffff",
    },
  },
  headerButtons: {
    textAlign: "center",
  },
  profile: {
    color: "black",
    position: "absolute",
    top: "20px",
    right: "16px",
  },
  logout: {
    color: "black",
    position: "absolute",
    top: "21px",
    right: "80px",
  },
  filter: {
    color: "black",
    position: "absolute",
    top: "700px",
    paddingLeft: "1000px",
    fontSize: "1rem",
  },
  addContainer: {
    display: "flex",
    paddingTop: theme.spacing(3),
    height: "50vh",
    width: "50%",
  },
  favContainer: {
    display: "inline",
    marginTop: "auto",
  },
  lockToBtm: { alignSelf: "flex-end", position: "absolute", bottom: 0 },

  locationList: {
    display: "flex",
    flexDirection: "row",
  },

  file: {
    content: "Select some files",
    display: "inline-block",
    background: "linear-gradient(top, #f9f9f9, #e3e3e3)",
    border: "1px solid #999",
    borderRadius: "3px",
    padding: "5px 8px",
    outline: "none",
    whiteSpace: "nowrap",
    fontWeight: 700,
    fontSize: "10pt",
    position: "relative",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  blogsContainer: {
    paddingTop: theme.spacing(3),
  },
  blogTitle: {
    fontWeight: 800,
    paddingBottom: theme.spacing(3),
  },
  card: {
    maxWidth: "100%",
  },
  media: {
    height: 150,
  },
  cardActions: {
    display: "flex",
    margin: "0 10px",
    justifyContent: "space-between",
  },
  contactMedia: {
    height: 150,
    width: 170,
    borderRadius: "50%",
  },
  author: {
    display: "flex",
  },
  paginationContainer: {
    display: "flex",
    justifyContent: "center",
  },
  subpages: {
    marginRight: "20px",
  },
  leftNum: {
    float: "left",
    alignItems: "center",
    justifyContent: "center",
  },
  rightNum: {
    display: "flex",
    marginLeft: "1ex",
    alignItems: "center",
    justifyContent: "center",
    textAlign: "justify",
  },
  addLocationPopup: {
    textAlign: "center",
    fontWeight: "bold",
    fontSize: "12px",
  },
}));
export default Styles;
