import * as api from "../api";

export const getLocation = () => async (dispatch) => {
  try {
    const { data } = await api.fetchLocation();

    dispatch({ type: "FETCH_ALL", payload: data });
  } catch (error) {
    console.log(error.message);
  }
};
export const getLocationDetails = (id) => async (dispatch) => {
  try {
    const { data } = await api.fetchLocationDetails(id);

    dispatch({ type: "FETCH_LOCATION", payload: { location: data } });
  } catch (error) {
    console.log(error.message);
  }
};
export const createLocation = (location) => async (dispatch) => {
  try {
    const { data } = await api.createLocation(location);

    dispatch({ type: "CREATE", payload: data });
  } catch (error) {
    console.log(error);
  }
};

export const likeLocation = (id) => async (dispatch) => {
  const user = String(localStorage.getItem("user"));
  console.log("user", user);
  try {
    const { data } = await api.likeLocation(id);

    dispatch({ type: "LIKE", payload: { data, user } });

    console.log("debug", data);
  } catch (error) {
    console.log(error);
  }
};

export const deleteLocation = (id) => async (dispatch) => {
  try {
    await api.deleteLocation(id);

    dispatch({ type: "DELETE", payload: id });
  } catch (error) {
    console.log(error);
  }
};
