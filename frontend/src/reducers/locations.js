export default (locations = [], action) => {
  switch (action.type) {
    case "DELETE":
      return locations.filter((location) => location.id !== action.payload);
    case "FETCH_ALL":
      return action.payload;

    case "FETCH_LOCATION":
      return locations.map((location) =>
        location.id === action.payload.id ? action.payload : location
      );
    case "LIKE":
      return locations.map((location) =>
        location.id === action.payload.id ? action.payload : location
      );
    case "CREATE":
      return [locations, action.payload];
    default:
      return locations;
  }
};
