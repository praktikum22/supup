import axios from "axios";
import { useEffect, useState } from "react";

const API = axios.create({ baseURL: "http://194.152.16.191:5000" });
const user = localStorage.getItem("user");
console.log(user);

export const fetchLocation = () => API.get("/location");

export const fetchLocationDetails = (id) => API.get(`/location/${id}`);

export const createLocation = (newLocation) =>
  API.post("/location", newLocation);

export const deleteLocation = (id) => API.delete(`/location/${id}`);

export const likeLocation = (id) =>
  API.patch(`/location/${id}/likeLocation`, { user });
