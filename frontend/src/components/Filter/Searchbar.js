import React, { useImperativeHandle, forwardRef } from 'react'
import { makeStyles } from '@material-ui/core/styles';
import SearchBar from "material-ui-search-bar";

const useStyles = makeStyles(theme => ({
  search: {
    maxWidth: 350,
    padding: theme.spacing(2)
  },
}))

const Searchbar = forwardRef((props, ref) => {
  const classes = useStyles();
  const [search, setSearch] = React.useState("");

  useImperativeHandle(ref, () => ({
    poslji2() {
      return search
    },
  }))

  return (

    <div className={classes.search}>
      <SearchBar
        placeholder="Išči..."
        onChange={(newValue) => {
          setSearch(newValue)
        }
        }
      />
    </div>
  );
}
)
export default Searchbar;