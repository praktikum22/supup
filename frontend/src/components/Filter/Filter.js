import React, {
  useEffect,
  useState,
  useCallback,
  forwardRef,
  useRef,
  useImperativeHandle,
} from "react";
import Dropdown from "react-mui-multiselect-dropdown";
import { Paper, Typography, makeStyles } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  error: {
    color: theme.palette.error.dark,
    fontSize: 13,
  },
  checkBox: {
    color: "#edcf07",
  },
}));
const DemoDropdown = forwardRef((props, ref) => {
  const [selectedOcena, setSelectedOcena] = React.useState("");
  const [selectedRegija, setSelectedRegija] = React.useState([]);
  const [selectedVoda, setSelectedVoda] = React.useState("");

  useImperativeHandle(ref, () => ({
    poslji() {
      return [[selectedOcena.ocena], [selectedRegija], [selectedVoda.voda]];
    },
  }));
  const [regije, setRegije] = useState([]);
  const [ocene, setOcene] = useState([]);
  const [vode, setVode] = useState([]);

  const vodaData = [
    { id: 1, voda: "reka" },
    { id: 2, voda: "morje" },
    { id: 3, voda: "jezero" },
  ];
  const oceneData = [
    { id: 1, ocena: "najmanj" },
    { id: 2, ocena: "največ" },
  ];
  const regijeData = [
    { id: 1, regija: "pomurska" },
    { id: 2, regija: "podravska" },
    { id: 3, regija: "koroška" },
    { id: 4, regija: "savinjska" },
    { id: 5, regija: "posavska" },
    { id: 6, regija: "zasavska" },
    { id: 7, regija: "osrednjeslovenska" },
    { id: 8, regija: "gorenjska" },
    { id: 9, regija: "goriška" },
    { id: 10, regija: "Obalno - kraška" },
    { id: 11, regija: "Primorsko - notranjska" },
    { id: 12, regija: "jugovzhodna Slovenija" },
  ];

  const populateData = () => {
    setOcene(oceneData);
    setRegije(regijeData);
    setVode(vodaData);
  };

  useEffect(() => {
    populateData();
  }, []);

  const classes = useStyles();

  return (
    <>
      <div>
        <Paper style={{ padding: "1em 10px" }}>
          <Typography
            variant="h6"
            style={{ color: "#0f7f85", marginTop: "1em", fontWeight: "600" }}
          >
            Ocena
          </Typography>
          <Dropdown
            data={ocene}
            fullWidth
            searchable
            searchPlaceHolder="ocene..."
            itemId="id"
            itemLabel="ocena"
            title="Izberi..."
            searchByValue="ocena"
            customStyles={{
              error: classes.error,
              checkBox: classes.checkBox,
            }}
            errorText="error"
            onItemClick={(ocena) => {
              setSelectedOcena(ocena);
            }}
          />
          <Typography
            variant="h6"
            style={{ color: "#0f7f85", marginTop: "1em", fontWeight: "600" }}
          >
            Regija
          </Typography>
          <Dropdown
            data={regije}
            fullWidth
            searchable
            multiple
            searchPlaceHolder="regije..."
            itemId="id"
            itemLabel="regija"
            title="Izberi..."
            searchByValue="regija"
            customStyles={{
              error: classes.error,
              checkBox: classes.checkBox,
            }}
            errorText="error"
            onItemClick={(regija) => {
              let rrr = [];
              regija.map((r) => rrr.push(r.regija));
              setSelectedRegija(rrr);
            }}
          />
          <Typography
            variant="h6"
            style={{ color: "#0f7f85", marginTop: "1em", fontWeight: "600" }}
          >
            Oblika vode
          </Typography>
          <Dropdown
            data={vode}
            fullWidth
            searchable
            searchPlaceHolder="vode..."
            itemId="id"
            itemLabel="voda"
            title="Izberi..."
            searchByValue="voda"
            customStyles={{
              error: classes.error,
              checkBox: classes.checkBox,
            }}
            errorText="error"
            onItemClick={(voda) => {
              setSelectedVoda(voda);
            }}
          />
        </Paper>
      </div>
    </>
  );
});
export default DemoDropdown;
