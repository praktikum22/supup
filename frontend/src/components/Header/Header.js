import React from "react";
import logo from "../../Images/sup-favicon.png";

function Header() {
  return (
    <a href="/">
      <img src={logo} alt="Logo" />
    </a>
  );
}

export default Header;
