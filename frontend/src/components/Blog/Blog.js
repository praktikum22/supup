import React from "react";
import Styles from "../../Styles/Styles";
import { BookmarkBorder } from "@material-ui/icons";
import {
  Typography,
  Box,
  Grid,
  Card,
  CardActionArea,
  CardActions,
  CardMedia,
  CardContent,
  Avatar,
} from "@material-ui/core";

function Blog() {
  const classes = Styles();
  return (
    <div>
      <Grid container spacing={3}>
        <Grid item xs={12} sm={6} md={4}>
          <Card className={classes.card}>
            <CardActionArea>
              <CardMedia
                className={classes.media}
                image="https://i.pinimg.com/564x/c2/c5/b9/c2c5b94881e6647c4f9ed5618f6d3c33.jpg"
                title="izbira Sup deske za začetnike"
              />
              <CardContent>
                <Typography gutterBottom variant="h5" component="h2">
                  Najbolj primerna SUP deska za začetnike
                </Typography>
                <Typography variant="body2" color="textSecondary" component="p">
                  »Kakšno sup desko kot začetnik potrebujem, na kaj moram biti
                  pozoren/a pri nakupu in kaj mi priporočate?« Eno je jasno -
                  sup deska za začetnike mora biti stabilna.
                </Typography>
              </CardContent>
            </CardActionArea>
            <CardActions className={classes.cardActions}>
              <Box className={classes.author}>
                <Avatar src="https://scontent.flju2-4.fna.fbcdn.net/v/t1.6435-9/71754069_2473777626051737_2929430393251692544_n.jpg?_nc_cat=100&ccb=1-6&_nc_sid=09cbfe&_nc_ohc=O3Gy2ja-7W4AX8Wu416&_nc_ht=scontent.flju2-4.fna&oh=00_AT9BBnegjkqlAnfmNzNmzi39uPPPFLUQ2nQWAojLyqS3CQ&oe=62A45E93" />
                <Box ml={2}>
                  <Typography variant="subtitle2" component="p">
                    Anja Borak
                  </Typography>
                  <Typography
                    variant="subtitle2"
                    color="textSecondary"
                    component="p"
                  >
                    May 12, 2022
                  </Typography>
                </Box>
              </Box>
              <Box>
                <BookmarkBorder />
              </Box>
            </CardActions>
          </Card>
        </Grid>
        <Grid item xs={12} sm={6} md={4}>
          <Card className={classes.card}>
            <CardActionArea>
              <CardMedia
                className={classes.media}
                image="https://www.divein.com/wp-content/uploads/Paddleboading/variations-of-stand-up-paddle-boards-420x280.jpg"
                title="Napihljiva Sup deska"
              />
              <CardContent>
                <Typography gutterBottom variant="h5" component="h2">
                  Ali je v redu, da pustim napihljivo SUP desko napihnjeno?
                </Typography>
                <Typography variant="body2" color="textSecondary" component="p">
                  Ali lahko pustite svojo napihljivo SUP desko napihnjeno? Ja,
                  zagotovo lahko! Ne pozabite na te nasvete, da ohranite desko v
                  najboljši obliki.
                </Typography>
              </CardContent>
            </CardActionArea>
            <CardActions className={classes.cardActions}>
              <Box className={classes.author}>
                <Avatar src="https://i.pinimg.com/736x/ad/2b/ef/ad2bef71bc9a71d0ed20d6fb64ffa794.jpg" />
                <Box ml={2}>
                  <Typography variant="subtitle2" component="p">
                    Sanja Fras
                  </Typography>
                  <Typography
                    variant="subtitle2"
                    color="textSecondary"
                    component="p"
                  >
                    May 12, 2022
                  </Typography>
                </Box>
              </Box>
              <Box>
                <BookmarkBorder />
              </Box>
            </CardActions>
          </Card>
        </Grid>
        <Grid item xs={12} sm={6} md={4}>
          <Card className={classes.card}>
            <CardActionArea>
              <CardMedia
                className={classes.media}
                image="https://redpaddleco.com/wp-content/uploads/2020/02/compact-gallery-6.jpg"
                title="Sup deska stabilnost"
              />
              <CardContent>
                <Typography gutterBottom variant="h5" component="h2">
                  Ali so napihljive SUP deske bolj stabilne?
                </Typography>
                <Typography variant="body2" color="textSecondary" component="p">
                  Ali so napihljive SUP deske bolj stabilne od desk iz epoksi?{" "}
                  <br /> Tukaj je vse, kar morate vedeti o njih.
                </Typography>
              </CardContent>
            </CardActionArea>
            <CardActions className={classes.cardActions}>
              <Box className={classes.author}>
                <Avatar src="https://i.pinimg.com/originals/bc/df/3c/bcdf3c3eb11ae8d027ab63eb1b6c9b2f.png" />
                <Box ml={2}>
                  <Typography variant="subtitle2" component="p">
                    Tina Kostantinovič
                  </Typography>
                  <Typography
                    variant="subtitle2"
                    color="textSecondary"
                    component="p"
                  >
                    May 12, 2022
                  </Typography>
                </Box>
              </Box>
              <Box>
                <BookmarkBorder />
              </Box>
            </CardActions>
          </Card>
        </Grid>
      </Grid>
    </div>
  );
}

export default Blog;
