import React, { useRef } from "react";
import Button from '@mui/material/Button';
import { Grid, Typography } from "@material-ui/core";
import Searchbar from "../Filter/Searchbar";
import Filter from "../Filter/Filter";
import Map from "./Map";
import SearchIcon from '@mui/icons-material/Search';
import Styles from "../../Styles/Styles";

export default function Proba() {
    const classes = Styles();
    const [data, setData] = React.useState([]);
    const [search, setSearch] = React.useState([]);
    const filterRef = useRef();
    const searchRef = useRef();

  return (
    <div className={classes.map}>
      <Grid alignItems="flex-start" container spacing={3}>
        <Grid container direction="column" item xs={4} xl={8} spacing={3}>
          <Grid item xs={12}>
            <Searchbar ref={searchRef} />
            <Button
                style={{ backgroundColor: "#d0b262" }}
                variant="contained"
                sx={{ ml: 2.3 }}
                onClick={() => {
                  setSearch(searchRef.current.poslji2());
                } }><SearchIcon />
              </Button>

          </Grid>
          <Grid item xs={12}>
            <Filter ref={filterRef} />
          </Grid>
          <Grid item xs={12}>
          <Button
                  fullWidth
                  style={{ backgroundColor: "#d0b262" }}
                  variant="contained"
                  sx={{ mt: 1, mb: 3 }}
                  onClick={() => {
                    setData(filterRef.current.poslji());
                  } }
                > Filtriraj</Button>
          </Grid>
        </Grid>
        <Grid container direction="column" item xs={6} spacing={3}>
          <Grid item xs={12}>
          <Map data={data} search={search} />
          <Typography variant="overline" className={classes.text1}>
            *Za dodajanje lokacij kliknite na zemljevid, kjer bi jo radi dodali.
          </Typography>
          </Grid>
        </Grid>
      </Grid>
    </div>
  );
}
