import React, { useRef } from "react";
import Button from "@mui/material/Button";
import { Grid, makeStyles, Container } from "@material-ui/core";
import Searchbar from "../Filter/Searchbar";
import Filter from "../Filter/Filter";
import Map from "../Map/Map";
import SearchIcon from "@mui/icons-material/Search";

const useStyles = makeStyles((theme) => ({
  map: {
    padding: theme.spacing(2),
  },
}));

function MapContainer() {
  const [data, setData] = React.useState([]);
  const [search, setSearch] = React.useState([]);
  const filterRef = useRef();
  const searchRef = useRef();
  const classes = useStyles();
  return (
    <>
      <Container></Container>
      <div className={classes.map}>
        <Grid container spacing={3}>
          <Grid alignItems="flex-start" item xs={8} sm={12} md={12}>
            <Grid container direction="column">
              <Grid item xs={12} sm={6}>
                <Searchbar ref={searchRef} />
              </Grid>
              <Grid item xs={12} sm={5}>
                <Button
                  style={{ backgroundColor: "#d0b262" }}
                  variant="contained"
                  sx={{ mb: 3, mt: 2, ml: 3 }}
                  onClick={() => {
                    setSearch(searchRef.current.poslji2());
                  }}
                >
                  <SearchIcon />
                </Button>
              </Grid>
              <Map data={data} search={search} />
            </Grid>
            <Grid item xs={12} sm={5}>
              <Filter ref={filterRef} />
              <Grid item xs={12} md={8}>
                <Button
                  fullWidth
                  style={{ backgroundColor: "#d0b262" }}
                  variant="contained"
                  sx={{ mt: 3, mb: 2 }}
                  onClick={() => {
                    setData(filterRef.current.poslji());
                  }}
                >
                  {" "}
                  Filtriraj
                </Button>
              </Grid>
            </Grid>
            <Grid item xs={12} sm={5}>
              <Map data={data} search={search} />
            </Grid>
          </Grid>
        </Grid>
      </div>
    </>
  );
}

export default MapContainer;
