import React, { useState } from "react";
import {
  MapContainer,
  TileLayer,
  useMapEvents,
  Popup,
  Polygon,
} from "react-leaflet";
import { Container } from "@material-ui/core";
import MarkerPopup from "./MarkerPopup/MarkerPopup";
import { useNavigate } from "react-router-dom";
import { useSelector } from "react-redux";
import Styles from "../../Styles/Styles";
import {
  obalnoKraska,
  primorskoNotranjska,
  jugovzhodnaSlovenija,
  goriska,
  posavska,
  zasavska,
  gorenjska,
  savinjska,
  koroska,
  podravska,
  pomurska,
  osrednjeslovenska,
} from "./regije";
const Map = ({ data, search, setCurrentId }) => {
  const classes = Styles();
  let ocena;
  let regija;
  let voda;
  if (data[0] == undefined || data[0] == "") {
    ocena = "";
  } else {
    ocena = data[0][0];
  }
  if (data[1] == undefined || data[1] == "") {
    regija = "";
  } else {
    regija = data[1][0];
  }
  if (data[2] == undefined || data[2] == "") {
    voda = "";
  } else {
    voda = data[2][0];
  }

  const [location, setLocation] = useState([]);
  const locations = useSelector((state) => state.locations);
  let l = locations;
  l = main(voda, regija, ocena, locations);
  if (search == "") {
  } else {
    l = searchLocations(search, l);
  }

  function searchLocations(search, locations) {
    let loc = [];
    for (let i = 0; i < locations.length; i++) {
      if (
        locations[i].title.toUpperCase().includes(search.toUpperCase()) ||
        locations[i].desc.toUpperCase().includes(search.toUpperCase()) ||
        locations[i].tags[0].toUpperCase().includes(search.toUpperCase())
      ) {
        loc.push(locations[i]);
      }
    }
    return loc;
  }

  function main(selectedVoda, selectedRegija, selectedOcena, locations) {
    let filtriraneLokacije = locations;
    if (selectedVoda) {
      filtriraneLokacije = water(selectedVoda, filtriraneLokacije);
    }
    if (selectedRegija) {
      filtriraneLokacije = regions(selectedRegija, filtriraneLokacije);
    }
    if (selectedOcena) {
      filtriraneLokacije = popularity(selectedOcena, filtriraneLokacije);
    }
    return filtriraneLokacije;
  }

  if (
    l.length < 1 &&
    (data[0] != undefined || data[1] != undefined || data[2] != undefined)
  ) {
    alert(
      "Prilagodite iskalne parametre za večji uspeh ali dopolnite naš seznam."
    );
  }

  function water(selectedVoda, locations) {
    let loc = [];
    for (let i = 0; i < locations.length; i++) {
      if (
        locations[i].tags[0].toUpperCase().includes(selectedVoda.toUpperCase())
      ) {
        if (loc.indexOf(locations[i]) == -1) {
          loc.push(locations[i]);
        }
      }
    }
    return loc;
  }

  function regions(selectedRegija, locations) {
    let loc = [];
    for (let i = 0; i < locations.length; i++) {
      for (let j = 0; j < selectedRegija.length; j++) {
        if (
          locations[i].tags[0]
            .toUpperCase()
            .includes(selectedRegija[j].toUpperCase())
        ) {
          if (loc.indexOf(locations[i]) == -1) {
            loc.push(locations[i]);
          }
        }
      }
    }
    return loc;
  }

  function popularity(selectedOcena, locations) {
    let vsota = 0;
    for (let i = 0; i < locations.length; i++) {
      vsota += locations[i].likes.length;
    }
    let povprecje = vsota / locations.length;
    if (selectedOcena == "največ") {
      return locations.filter((location) => location.likes.length >= povprecje);
    } else if (selectedOcena == "najmanj") {
      return locations.filter((location) => location.likes.length <= povprecje);
    }
  }

  return (
   
      <MapContainer
        center={[46.1552541, 15.006483]}
        zoom={8.3}
        zoomSnap={0.1}
        scrollWheelZoom={true}
      >
        <TileLayer
          attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        />
        {l.map((location) => (
          <MarkerPopup
            key={location._id}
            location={location}
            setCurrentId={setCurrentId}
          />
        ))}
        {regija ? regija.map((r) => <Obroba regija={r} />) : ""}
        <Dodajanje location={location} />
      </MapContainer>
   
  );
};

function Dodajanje({ locationxy }) {
  const classes = Styles();
  const navigate = useNavigate();
  const [location, setLocation] = useState(
    locationxy === undefined ? null : locationxy
  );

  const toAddLocation = () => {
    navigate("/add-location", {
      state: { locationx: location[0], locationy: location[1] },
    });
  };

  useMapEvents({
    click(e) {
      let locationx = e.latlng.lat;
      let locationy = e.latlng.lng;
      setLocation([locationx, locationy]);
    },
  });

  return location === null ? null : (
    <Popup position={location}>
      <a
        onClick={() => {
          toAddLocation();
        }}
      >
        Dodaj lokacijo
      </a>
    </Popup>
  );
}

function Obroba({ regija }) {
  if (regija == "pomurska") {
    return <Polygon positions={pomurska} pathOptions={{ color: "#5AB6B9" }} />;
  }

  if (regija == "podravska") {
    return <Polygon positions={podravska} pathOptions={{ color: "#4b9799" }} />;
  }
  if (regija == "koroška") {
    return <Polygon positions={koroska} pathOptions={{ color: "#3a7678" }} />;
  }
  if (regija == "savinjska") {
    return <Polygon positions={savinjska} pathOptions={{ color: "#4b9799" }} />;
  }
  if (regija == "posavska") {
    return <Polygon positions={posavska} pathOptions={{ color: "#3a7678" }} />;
  }
  if (regija == "zasavska") {
    return <Polygon positions={zasavska} pathOptions={{ color: "#5AB6B9" }} />;
  }
  if (regija == "osrednjeslovenska") {
    return (
      <Polygon
        positions={osrednjeslovenska}
        pathOptions={{ color: "#76d3d6" }}
      />
    );
  }
  if (regija == "gorenjska") {
    return <Polygon positions={gorenjska} pathOptions={{ color: "#5AB6B9" }} />;
  }
  if (regija == "goriška") {
    return <Polygon positions={goriska} pathOptions={{ color: "#3a7678" }} />;
  }
  if (regija == "Obalno - kraška") {
    return (
      <Polygon positions={obalnoKraska} pathOptions={{ color: "#76d3d6" }} />
    );
  }
  if (regija == "Primorsko - notranjska") {
    return (
      <Polygon
        positions={primorskoNotranjska}
        pathOptions={{ color: "#5AB6B9" }}
      />
    );
  }
  if (regija == "jugovzhodna Slovenija") {
    return (
      <Polygon
        positions={jugovzhodnaSlovenija}
        pathOptions={{ color: "#3a7678" }}
      />
    );
  }
}

export default Map;
