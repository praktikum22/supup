import React from "react";
import { Marker, Popup } from "react-leaflet";
import Location from "../../Locations/Location/Location";
import { useDispatch } from "react-redux";
import Heart from "../../LocationDetails/Heart";
import { Link } from "react-router-dom";

const MarkerPopup = ({ location }) => {
  const dispatch = useDispatch();
  return (
    <Marker
      key={location.id}
      position={[location.locationx, location.locationy]}
      eventHandlers={{
        mouseover: (event) => event.target.openPopup(),
      }}
    >
     <Popup autoPan={false}>
        <img src={location.selectedFile ||
            "https://user-images.githubusercontent.com/194400/49531010-48dad180-f8b1-11e8-8d89-1e61320e1d82.png"} style={{ width: "200px" }} /><br />
        <Link style={{ color: '#0f7f85', fontWeight: "600" }}
          to={`/location/${location._id}`} state={location}>
          {location.title}
        </Link><br />
        {location.tags} <br />
        <Heart id={location.id} change={false} array={location.likes}/>
      </Popup>
    </Marker>
  );
};
export default MarkerPopup;
// <Heart id={location._id} change={false} array={location.likes} />
