import React from 'react';
import { useState } from "react";
import { auth } from "../Subpages/firebase_config";
import {
  createUserWithEmailAndPassword,
  signInWithEmailAndPassword,
  onAuthStateChanged,
  signOut,
} from "firebase/auth";

function Racun() {
  const [registerEmail, setRegisterEmail] = useState("");
  const [registerPassword, setRegisterPassword] = useState("");
  const [loginEmail, setLoginEmail] = useState("");
  const [loginPassword, setLoginPassword] = useState("");

  const [user, setUser] = useState({});

  onAuthStateChanged(auth, (currentUser) => {
    setUser(currentUser);
  });

  const register = async () => {
    try {
      const user = await createUserWithEmailAndPassword(
        auth,
        registerEmail,
        registerPassword
      );
      console.log(user);
    } catch (error) {
      console.log(error.message);
    }
  };

  const login = async () => {
    try {
      const user = await signInWithEmailAndPassword(
        auth,
        loginEmail,
        loginPassword
      );
      console.log(user);
    } catch (error) {
      console.log(error.message);
    }
  };

  const logout = async () => {
    await signOut(auth);
  };
  return (
    <div className="App">


      {user ? (
        <div>
          <h4> User Logged In: </h4>
          {user.email}<br />
          <button onClick={logout}> Sign Out </button>

        </div>
      ) : (
        <div>
          <div>
            <h3> Register User </h3>
            <input
              placeholder="Email..." type="email"
              onChange={(event) => {
                setRegisterEmail(event.target.value);
              }} />
            <input
              placeholder="Password..." type="password"
              onChange={(event) => {
                setRegisterPassword(event.target.value);
              }} />
            <button onClick={register}> Create User</button>
          </div>
          <div>
            <h3> Login </h3>
            <input
              placeholder="Email..." type="email"
              onChange={(event) => {
                setLoginEmail(event.target.value);
              }} />
            <input
              placeholder="Password..." type="password"
              onChange={(event) => {
                setLoginPassword(event.target.value);
              }} />
            <button onClick={login}> Login</button>
          </div>
        </div>
      )}
      <br/>
      </div>
      
  );
}
export default Racun;