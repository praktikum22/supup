import React from "react";
import Styles from "../../Styles/Styles";
import { BookmarkBorder } from "@material-ui/icons";
import {
  Typography,
  Grid,
  Card,
  CardActionArea,
  CardMedia,
  CardContent
} from "@material-ui/core";

function ContactUs() {
  const classes = Styles();
  return (
    <div>
      <Grid align="center">
        <Grid item xs={12} sm={6} md={4}>
          <Card className={classes.card} align="center">
            <CardActionArea>
              <CardMedia
                className={classes.contactMedia}
                image="https://i.pinimg.com/564x/9f/02/2e/9f022e5a854a5297e89d312b5546b512.jpg"
                title="ContactUs"
              />
              <CardContent>
                <Typography gutterBottom variant="h5" component="h2" align="center">
                  Email
                </Typography>
                <Typography variant="body2" color="textSecondary" component="p">
                  Kontaktiraje nas na{" "}
                  <br /> <a href = "mailto: supup.info@gmail.com"> supup.info@gmail.com </a>
                </Typography>
              </CardContent>
            </CardActionArea>
          </Card>
        </Grid>
      </Grid>
    </div>
  );
}

export default ContactUs;
