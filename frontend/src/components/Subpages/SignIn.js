import * as React from "react";
import Avatar from "@mui/material/Avatar";
import Button from "@mui/material/Button";
import CssBaseline from "@mui/material/CssBaseline";
import TextField from "@mui/material/TextField";
import FormControlLabel from "@mui/material/FormControlLabel";
import Checkbox from "@mui/material/Checkbox";
import Link from "@mui/material/Link";
import Paper from "@mui/material/Paper";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import LockTwoToneIcon from "@mui/icons-material/LockTwoTone";
import Typography from "@mui/material/Typography";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import { useState } from "react";
import {
  sendEmailVerification,
  signInWithEmailAndPassword,
  onAuthStateChanged,
  signOut,
} from "firebase/auth";
import { auth } from "./firebase_config";

import { useNavigate } from "react-router-dom";

function Copyright(props) {
  return (
    <Typography
      variant="body2"
      color="text.secondary"
      align="center"
      {...props}
    >
      {"Copyright © "}
      <Link color="inherit" href="/">
        SupUp
      </Link>{" "}
      {new Date().getFullYear()}
      {"."}
    </Typography>
  );
}

const theme = createTheme();

export default function SignInSide() {
  const [loginEmail, setLoginEmail] = useState("");
  const [loginPassword, setLoginPassword] = useState("");
  let navigate = useNavigate();
  const [user, setUser] = useState({});

  const login = async () => {
    try {
      const user = await signInWithEmailAndPassword(
        auth,
        loginEmail,
        loginPassword
      ).then((user) => {
        setUser(user);
        if (user.user.emailVerified) {
        } else {
          sendEmailVerification(user.user).then((a) => {
            signOut(auth);
          });
          alert("Prosim validirajte email, ki smo vam ga ponovno poslali.");
        }
      });
      localStorage.setItem("user", auth.currentUser.uid);
      localStorage.setItem("email", auth.currentUser.email);
      let path = "/";
      navigate(path);
    } catch (error) {
      console.log(error.message);
    }
  };

  return (
    <ThemeProvider theme={theme}>
      <Grid container component="main" sx={{ height: "100vh" }}>
        <CssBaseline />
        <Grid
          item
          xs={false}
          sm={4}
          md={7}
          sx={{
            backgroundImage:
              "url(https://wallpaperaccess.com/full/6313894.jpg)",
            backgroundRepeat: "no-repeat",
            backgroundColor: (t) =>
              t.palette.mode === "light"
                ? t.palette.grey[50]
                : t.palette.grey[900],
            backgroundSize: "cover",
            backgroundPosition: "center",
          }}
        />
        <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
          <Box
            sx={{
              my: 8,
              mx: 4,
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
            }}
          >
            <Avatar sx={{ m: 1, backgroundColor: '#0f7f85'}}>
              <LockTwoToneIcon />
            </Avatar>
            <Typography component="h1" variant="h5">
              Vpis
            </Typography>
            <Box component="form" noValidate sx={{ mt: 1 }}>
              <TextField
                margin="normal"
                required
                fullWidth
                id="email"
                label="Email naslov"
                name="email"
                autoComplete="email"
                autoFocus
                onChange={(event) => {
                  setLoginEmail(event.target.value);
                }}
              />
              <TextField
                margin="normal"
                required
                fullWidth
                name="password"
                label="Geslo"
                type="password"
                id="password"
                autoComplete="current-password"
                onChange={(event) => {
                  setLoginPassword(event.target.value);
                }}
              />
              <FormControlLabel
                control={<Checkbox value="remember" style={{ color: '#0f7f85'}} />}
                label="Zapomni si me"
              />
              <Button
                fullWidth
                variant="contained"
                sx={{ mt: 3, mb: 2 }}
                onClick={login}
                style={{ backgroundColor: '#0f7f85'}}
              >
                Vpiši se
              </Button>
              <Grid container>
                <Grid item xs>
                  <Link href="/forgot-password" variant="body2" style={{ color: '#0f7f85'}}>
                    Pozabili geslo?
                  </Link>
                </Grid>
                <Grid item>
                  <Link href="/register" variant="body2" style={{ color: '#0f7f85'}}>
                    {"Še nimate računa? Registrirajte se"}
                  </Link>
                </Grid>
              </Grid>
              <Copyright sx={{ mt: 5 }} />
            </Box>
          </Box>
        </Grid>
      </Grid>
    </ThemeProvider>
  );
}
