import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";
const firebaseConfig = {
    apiKey: "AIzaSyAcntqIaNvDcmW-4OutiLs97ntS7IBVgmI",
    authDomain: "supup-auth.firebaseapp.com",
    projectId: "supup-auth",
    storageBucket: "supup-auth.appspot.com",
    messagingSenderId: "843617462477",
    appId: "1:843617462477:web:9e426f1866ed1707b1a184",
};

const app = initializeApp(firebaseConfig);

export const auth = getAuth(app);