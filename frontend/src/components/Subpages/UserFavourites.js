import React, { useState } from "react";
import { onAuthStateChanged } from "firebase/auth";
import { auth } from "./firebase_config";
import { Navigate, Link } from "react-router-dom";
import { useSelector } from "react-redux";
import FavoriteIcon from "@mui/icons-material/Favorite";
import {
  makeStyles,
  Container,
  Card,
  CardContent,
  CardMedia,
  Typography,
} from "@material-ui/core";
import Location from "../Locations/Location/Location";

const useStyles = makeStyles((theme) => ({
  text: {
    margin: theme.spacing(0, 0, 0.5),
  },
  cardContent: {
    padding: theme.spacing(2, 0, 0, 0),
  },
}));

export function UserFavourites(props) {
  const classes = useStyles();

  const locations = useSelector((state) => state.locations);
  const [user, setUser] = useState({});

  onAuthStateChanged(auth, (currentUser) => {
    setUser(currentUser);
  });
  return (
    <>
      {!user ? (
        <>
          <Navigate to="/" />
        </>
      ) : (
        <Container sx={{ py: 3 }} maxWidth="md">
          <Card>
            <CardMedia align="center">
              <FavoriteIcon style={{ color: "#0f7f85" }} />
            </CardMedia>
            <CardContent className={classes.cardContent}>
              {" "}
              <Typography
                className={classes.text}
                color="textSecondary"
                variant="subtitle1"
                align="center"
              >
                Moje priljubljene lokacije:
              </Typography>
              {locations.map((location) => (
                <Lokacija
                  location={location}
                  user={user.uid}
                  setCurrentId={location._id}
                />
              ))}
            </CardContent>
          </Card>
        </Container>
      )}
    </>
  );
}

function Lokacija({ location, user }) {
  if (location.likes.includes(user)) {
    return (
      <>
        <Location location={location} />
      </>
    );
  } else {
    return "";
  }
}
