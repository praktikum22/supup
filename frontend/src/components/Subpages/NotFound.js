import * as React from "react";
import { Typography } from "@material-ui/core";

const B = (props) => <Typography style={{fontWeight: "bold", fontSize: 80}}>{props.children}</Typography>
const NotFound = () => {
  return <div>
    <Typography align="center" variant="h6" color="text.secondary" paragraph>
    <br />
    <img
    src="https://t4.ftcdn.net/jpg/04/29/70/79/360_F_429707932_I5UIXsnBBBD1z4PsWmwzVbMpBWaYVKAe.jpg"
    alt="404 page"
    width={500} height={350}
    justifyContent="center"
    />
    <br />
    <B>404</B> <br />
    Stran ne obstaja <br /> Žal strani, ki jo iščete, ni bilo mogoče najti
    </Typography>
    </div>;
};

export default NotFound;
