import React from "react";
import { Grid, makeStyles, Container } from "@material-ui/core";
import Profile from "../Subpages/Profile";
import { UserFavourites } from "./UserFavourites";

const useStyles = makeStyles((theme) => ({
  fav: {
    padding: theme.spacing(2),
    height: "50%",
    justifyContent: "center",
    flexDirection: "row",
  },
}));

function ProfileContainer() {
  const classes = useStyles();

  return (
    <div className={classes.fav}>
      <Container>
        <Grid item xs={6} sm={2} md={8} lg={12} xl={12}>
          <Profile />
        </Grid>
        &nbsp;
        <Grid item xs={8} sm={12} md={8} lg={12} xl={12}>
          <UserFavourites />
        </Grid>
      </Container>
    </div>
  );
}

export default ProfileContainer;
