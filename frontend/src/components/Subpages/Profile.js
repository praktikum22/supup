import React from "react";
import { useState } from "react";
import { onAuthStateChanged } from "firebase/auth";
import { auth } from "./firebase_config";
import { Navigate } from "react-router-dom";
import {
  makeStyles,
  Card,
  CardContent,
  CardMedia,
  Avatar,
  Typography,
} from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  text: {
    margin: theme.spacing(0, 0, 0.5),
  },
  avatar: {
    verticalAlign: "middle",
    marginRight: theme.spacing(0.5),
  },
  large: {
    width: theme.spacing(12),
    height: theme.spacing(12),
    margin: theme.spacing(2, 2, 0),
  },
  card: {
    borderRadius: 15,
    maxWidth: "270px",
    minWidth: "270px",
    height: "280px",
    padding: "20px",
    backgroundColor: theme.palette.background.card,
  },
  cardContent: {
    padding: theme.spacing(2, 0, 0, 0),
  },
}));

export default function UserCard(props) {
  const classes = useStyles();

  const [user, setUser] = useState({});

  onAuthStateChanged(auth, (currentUser) => {
    setUser(currentUser);
  });
  return (
    <>
      {!user ? (
        <>
          <Navigate to="/" />
        </>
      ) : (
        <Card
          variant="outlined"
          className={classes.card}
          style={{ display: "inline-block" }}
        >
          <CardMedia align="center">
            <Avatar
              alt="User profile"
              src="https://source.unsplash.com/random"
              className={classes.large}
            />
          </CardMedia>
          <CardContent className={classes.cardContent}>
          {" "}
            <Typography
              className={classes.text}
              color="textSecondary"
              variant="subtitle1"
              align="center"
            >
              Email: {user.email}
            </Typography>
            {" "}
          </CardContent>
        </Card>
      )}
    </>
  );
}
