import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import SurfingIcon from '@mui/icons-material/Surfing';
import CssBaseline from '@mui/material/CssBaseline';
import Stack from '@mui/material/Stack';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { createTheme, ThemeProvider } from '@mui/material/styles';

const theme = createTheme();

export default function About() {
  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <AppBar position="relative" style={{backgroundColor: "#12949B"}}>
        <Toolbar>
          <SurfingIcon sx={{ mr: 2 }} />
          <Typography variant="h6" color="inherit" noWrap>
            O nas
          </Typography>
        </Toolbar>
      </AppBar>
      <main>
        {/* Hero unit */}
        <Box
          sx={{
            bgcolor: 'background.paper',
            pt: 8,
            pb: 6,
          }}
        >
          <Container maxWidth="sm">
            <Typography
              component="h1"
              variant="h2"
              align="center"
              color="text.primary"
              gutterBottom
            >
              O Sup Up
            </Typography>
            <Typography variant="h6" align="center" color="text.secondary" paragraph>
                Supanje z veliko hitrostjo osvaja svet in postaja najbolj priljubljena vodna aktivnost.
                Izletnikovanje s supom in popolna sprostitev v naravi sta dva najpogosteje omenjena razloga,
                zaradi katerih se ljudje s supanjem ukvarjajo tudi izven poletnih mesecev.
                <br />
                <br />
                Supanje je sproščujoča rekreacija na vodi, ki ne pozna telesnih ali starostnih omejitev.
                <br/>
                <br />
                SupUp spreminja suparje v samozavestne raziskovalce, ki znajo samostojno ustvarjati zanimive in
                varne sup avanture in raziskujejo svet na aktiven, planetu prijazen način.

            </Typography>
            <Stack
              sx={{ pt: 4 }}
              direction="row"
              spacing={2}
              justifyContent="center"
            >
            <img 
                src="https://www.supup.fitness/wp-content/uploads/2021/07/sup-up-logo-dark-gray-spaced.png"
                width={150} height={150}
                alt="about-us"
            />
            </Stack>
          </Container>
        </Box>
        <Container sx={{ py: 8 }} maxWidth="md">
         
        </Container>
      </main>
    </ThemeProvider>
  );
}