import React, { useState }  from 'react';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import Link from '@mui/material/Link';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { useNavigate } from "react-router-dom";
import {
  createUserWithEmailAndPassword,
  onAuthStateChanged,
  sendEmailVerification,
  signOut
} from "firebase/auth";
import { auth } from "./firebase_config";

const theme = createTheme();

export default function SignUp() {
  const [registerEmail, setRegisterEmail] = useState("");
  const [registerPassword, setRegisterPassword] = useState("");
  const [checked, setChecked] = useState("");
  const [disable, setDisable] = React.useState(true);

  const [user, setUser] = useState({});


  const check = (e) => {
    const checked = e.target.checked;
    if (checked) {
      setChecked(true);
      setDisable(false);
    } else {
      setChecked(false);
      setDisable(true);
    }
  };

  onAuthStateChanged(auth, (currentUser) => {
    setUser(currentUser);
  });

  const navigate = useNavigate();
  const register =async () => {
    try {
      createUserWithEmailAndPassword(
        auth,
        registerEmail,
        registerPassword
      ).then(user => {
        sendEmailVerification(user.user);
        signOut(auth);
        alert("Pred prijavo validirajte email, ki ste ga prejeli. Pogosto se znajde med vsiljeno pošto.")
        navigate("/sign-in")
      });

    } catch (error) {
      console.log(error.message);
    }
  };

  return (
    <ThemeProvider theme={theme}>
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <Box
          sx={{
            marginTop: 8,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
          }}
        >
          <Avatar sx={{ m: 1, backgroundColor: '#0f7f85'}}>
            <LockOutlinedIcon/>
          </Avatar>
          <Typography component="h1" variant="h5">
            Registracija
          </Typography>
          <Box component="form" noValidate sx={{ mt: 3 }}>
            <Grid container spacing={2}>
              <Grid item xs={12} sm={6}>
                <TextField
                  autoComplete="given-name"
                  name="firstName"
                  required
                  fullWidth
                  id="firstName"
                  label="Ime"
                  autoFocus
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  required
                  fullWidth
                  id="lastName"
                  label="Priimek"
                  name="lastName"
                  autoComplete="family-name"
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  required
                  fullWidth
                  id="email"
                  label="Email naslov"
                  name="email"
                  autoComplete="email"
                  onChange={(event) => {
                    setRegisterEmail(event.target.value);
                  }}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  required
                  fullWidth
                  name="password"
                  label="Geslo"
                  type="password"
                  id="password"
                  autoComplete="new-password"
                  onChange={(event) => {
                    setRegisterPassword(event.target.value);
                  }}
                />
              </Grid>
              <br />
              <Grid item xs={12}>
                <FormControlLabel
                  control={<Checkbox value="allowExtraEmails" onClick={(e) => { check(e); }} style={{ color: '#0f7f85'}} />}
                  label="Z registracijo potrjujete, da sprejemate naše pogoje in določila ter da ste prebrali našo politiko zasebnosti."
                />
              </Grid>
            </Grid>
            <Button
              fullWidth
              variant="contained"
              sx={{ mt: 3, mb: 2 }}
              onClick={register}
              disabled={disable}
              style={{ backgroundColor: '#0f7f85'}}
            >
              Prijava
            </Button>
            <br />
            <br />
            <Grid container justifyContent="flex-end">
              <Grid item>
                <Link href="/sign-in" variant="body2" style={{ color: '#0f7f85'}}>
                  Že imaš račun? Prijavi se
                </Link>
              </Grid>
            </Grid>
          </Box>
        </Box>
      </Container>
    </ThemeProvider>
  );
}