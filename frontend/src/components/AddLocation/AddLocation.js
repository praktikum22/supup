import React, { useState } from "react";
import {
  TextField,
  Button,
  Typography,
  Paper,
  Container,
  Grid,
  Box,
} from "@material-ui/core";
import FileBase from "react-file-base64";
import Styles from "../../Styles/Styles";
import { useDispatch } from "react-redux";
import { createLocation } from "../../actions/locations";
import { Navigate } from "react-router-dom";
import { onAuthStateChanged } from "firebase/auth";
import { auth } from "../Subpages/firebase_config";
import { useLocation } from 'react-router-dom';

const clear = () => { };
const AddLocation = () => {

  const location = useLocation();
  const [locationData, setLocationData] = useState({
    creator: "",
    locationx: "",
    locationy: "",
    title: "",
    message: "",
    tags: "",
    selectedFile: "",
  });
  const dispatch = useDispatch();

  const handleSubmit = (e) => {
    e.preventDefault();

    dispatch(createLocation(locationData));
    clear();
    alert("Lokacija dodana, hvala za vaš prispevek.")
  };
  const classes = Styles();
  const [user, setUser] = useState({});
  onAuthStateChanged(auth, (currentUser) => {
    setUser(currentUser);
  });

  return (
    <>
      {!user ? (<><Navigate to="/" /></>) : (
        <Container maxWidth="md" className={classes.addContainer}>
          <Typography component="h1" variant="h5">
            Dodaj lokacijo
          </Typography>
          <Paper>
            <form autoComplete="off" noValidate onSubmit={handleSubmit}>
              <Grid container spacing={2}>
                <Grid item xs={12} sm={6}>
                  <TextField
                    name="desc"
                    variant="outlined"
                    label="Title"
                    fullWidth
                    value={Location.title}
                    onChange={(e) =>
                      setLocationData({ ...locationData, title: e.target.value })
                    }
                  />
                </Grid>
                <Grid item xs={12} sm={6}>
                  <TextField
                    name="description"
                    variant="outlined"
                    label="Description"
                    fullWidth
                    value={Location.desc}
                    onChange={(e) =>
                      setLocationData({ ...locationData, desc: e.target.value })
                    }
                  />
                </Grid>
                <Grid item xs={12} sm={6}>
                  <TextField
                    name="locationx"
                    variant="outlined"
                    label="Location X"
                    fullWidth
                    value={location.state.locationx}
                    onChange={(e) =>
                      setLocationData({
                        ...locationData,
                        locationx: e.target.value,
                      })
                    }
                  />
                </Grid>
                <Grid item xs={12} sm={6}>
                  <TextField
                    name="locationy"
                    variant="outlined"
                    label="Location Y"
                    fullWidth
                    value={location.state.locationy}
                    onChange={(e) =>
                      setLocationData({
                        ...locationData,
                        locationy: e.target.value,
                      })
                    }
                  />
                </Grid>
                <Grid item xs={12} sm={6}>
                  <TextField
                    name="tags"
                    variant="outlined"
                    label="Tags"
                    fullWidth
                    value={Location.tags}
                    onChange={(e) =>
                      setLocationData({ ...locationData, tags: e.target.value })
                    }
                  />
                </Grid>
                <Grid item xs={12} sm={6}>
                  <div className={classes.file}>
                    <FileBase
                      type="file"
                      multiple={false}
                      onDone={({ base64 }) =>
                        setLocationData({ ...locationData, selectedFile: base64 })
                      }
                    />
                  </div>
                </Grid>
              </Grid>
              <br />
              <br />
              <Box textAlign="center">
                <Button type="submit" onClick={() => setLocationData({
                  ...locationData,
                  locationy: location.state.locationy,
                  locationx: location.state.locationx,
                  creator: user.email.slice(0, user.email.indexOf("@"))
                })
                }
                  variant="contained">
                  Submit
                </Button>

                <Button variant="contained" onClick={clear}>
                  Clear
                </Button>
              </Box>
            </form>
          </Paper>
        </Container>
      )}</>
  );
};
export default AddLocation;
