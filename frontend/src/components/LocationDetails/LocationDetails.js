import React, { useEffect } from "react";
import Styles from "../../Styles/Styles";
import { useParams, useLocation } from "react-router-dom";
import {
  Container,
  Typography,
  Box,
  Button,
  CircularProgress,
  Alert,
} from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import {
  deleteLocation,
  likeLocation,
  getLocationDetails,
} from "../../actions/locations";
import Heart from "./Heart";

function LocationDetails() {
  const { id } = useParams();
  const { state } = useLocation();
  const locations = useSelector((state) => state.locations);
  const location = locations.find((location) => location._id === id);
  const dispatch = useDispatch();
  const classes = Styles();
  const user = localStorage.getItem("user");
  const email = localStorage.getItem("email");

  const handleDelete = async () => {
    dispatch(deleteLocation(id));
  };
  let dodal = false;
  if (email != null && email.slice(0, email.indexOf("@")) == location.creator) {
    dodal = true;
  }

  return !locations.length ? (
    <div
      style={{
        alignItems: "center",
        display: "flex",
        justifyContent: "center",
        height: "100vh",
        width: "100vw",
      }}
    >
      <CircularProgress />
    </div>
  ) : (
    <Container maxWidth="md" className={classes.addContainer}>
      {location && (
        <div>
          <img
            src={
              location.selectedFile ||
              "https://user-images.githubusercontent.com/194400/49531010-48dad180-f8b1-11e8-8d89-1e61320e1d82.png"
            }
            title={location.title}
            width="400"
            height="250"
            object-fit="cover"
          />
          <div>
            <Typography gutterBottom variant="h5" component="h2">
              {location.title}
            </Typography>
            <Typography variant="h9">{location.creator}</Typography>
            <Typography variant="body2">{location.desc}</Typography>
          </div>
          <Typography variant="body2" color="textSecondary" component="h2">
            {location.tags.map((tag) => `#${tag} `)}
          </Typography>

          <Typography variant="body2" color="textSecondary" component="p">
            {location.message}
          </Typography>
          <Box className={classes.favContainer}>
            <Heart id={location._id} array={location.likes} change={true} />
          </Box>
          {dodal ? <Button onClick={handleDelete}>Delete</Button> : ""}
        </div>
      )}
    </Container>
  );
}

export default LocationDetails;
