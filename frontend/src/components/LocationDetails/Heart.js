import React, { useState } from "react";
import { FavoriteBorder, Favorite } from "@mui/icons-material";
import { likeLocation } from "../../actions/locations";
import { useDispatch } from "react-redux";
import { Typography, Checkbox, Alert } from "@mui/material";
import { Link } from "react-router-dom";
import { confirm } from "react-confirm-box";

const Heart = ({ id, change, array }) => {
  const dispatch = useDispatch();
  const user = localStorage.getItem("user");
  let checked = true;
  const [like, setLike] = useState(false);
  if (user) {
    if (array?.includes(user)) {
      checked = true;
    } else {
      checked = false;
    }
  }
  async function likeLoc() {
    if (change) {
      await dispatch(likeLocation(id));
      setLike(!like);
      console.log(id);
      window.location.reload();
    }
  }

  return (
    <>
      {user ? (
        <Typography>
          <Checkbox
            checked={checked}
            label="heart"
            onClick={likeLoc}
            icon={<FavoriteBorder />}
            checkedIcon={<Favorite />}
            style={{ color: "#0f7f85" }}
          />
          {array.length}
        </Typography>
      ) : (
        <Typography>
          <Link to={`/sign-in`}>
            <Checkbox
              checked={true}
              label="heart"
              icon={<FavoriteBorder />}
              checkedIcon={<Favorite />}
            />
          </Link>
          {array?.length}
        </Typography>
      )}
    </>
  );
};
export default Heart;
