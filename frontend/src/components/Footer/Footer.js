import React from 'react';
import './Footer.css';

function Footer() {
    return (
        <div className="noga">
            &copy; 2022 SupUp
        </div>
    )
}

export default Footer;