import React from "react";
import { Link } from "react-router-dom";
import { useState } from "react";
import { onAuthStateChanged } from "firebase/auth";
import { auth } from "../Subpages/firebase_config";
import Styles from "../../Styles/Styles";

function Subpages() {
  const classes = Styles();
  const [user, setUser] = useState({});

  onAuthStateChanged(auth, (currentUser) => {
    setUser(currentUser);
  });

  return (
    <header>
      <ul className={classes.headerButtons}>
        <Link className={classes.link} to="about">
          O NAS
        </Link>
        &nbsp;&nbsp;
        <Link className={classes.link} to="/locations">
          SEZNAM VSEH LOKACIJ
        </Link>
        &nbsp;&nbsp;
        {user ? (
          <>
            <Link className={classes.link} to="/profile">
              MOJ PROFIL
            </Link>
            &nbsp;&nbsp;
          </>
        ) : (
          <>
            <Link className={classes.link} to="sign-in">
              VPIS
            </Link>{" "}
            &nbsp;&nbsp;
            <Link className={classes.link} to="/register">
              REGISTRACIJA
            </Link>{" "}
            &nbsp;&nbsp;
          </>
        )}
      </ul>
    </header>
  );
}
export default Subpages;
