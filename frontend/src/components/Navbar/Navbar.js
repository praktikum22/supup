import React from "react";
import LogoutIcon from "@mui/icons-material/Logout";
import Tooltip from "@mui/material/Tooltip";
import IconButton from "@mui/material/IconButton";
import Styles from "../../Styles/Styles";
import Subpages from "./Subpages";
import Header from "../Header/Header";
import { AppBar, Toolbar, Box, Avatar, Grid } from "@material-ui/core";
import { useState } from "react";
import { onAuthStateChanged, signOut } from "firebase/auth";
import { auth } from "../Subpages/firebase_config";
import { useNavigate } from "react-router-dom";

function Navbar() {
  const classes = Styles();
  const [user, setUser] = useState({});
  let navigate = useNavigate();

  onAuthStateChanged(auth, (currentUser) => {
    setUser(currentUser);
  });
  const logout = async () => {
    await signOut(auth);
    localStorage.removeItem("user");
    localStorage.removeItem("email");

    let path = "/";
    navigate(path);
  };

  return (
    <AppBar className={classes.appBar} color="inherit" position="static">
      <Toolbar>
        <Header />
        <Grid item xs={10} sm={10} md={10}>
          <Subpages />
        </Grid>
        <Box className={classes.profile}>
          <a href="/profile">
            <Avatar src="https://source.unsplash.com/random" />
          </a>
        </Box>
        {user ? (
          <div>
            <Box className={classes.logout}>
              <Tooltip title="Izpis">
                <IconButton onClick={logout}>
                  <LogoutIcon />
                </IconButton>
              </Tooltip>
            </Box>
          </div>
        ) : (
          <Box className={classes.profile}>
            <a href="/sign-in">
              <Avatar src={user} />
            </a>
          </Box>
        )}
      </Toolbar>
    </AppBar>
  );
}
export default Navbar;
