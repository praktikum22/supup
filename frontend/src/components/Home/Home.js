import React from "react";
import Styles from "../../Styles/Styles";
import MapContainer from "../Map/MapContainer";
import Blog from "../Blog/Blog";
import { Typography, Box, Container, Grid, Card } from "@material-ui/core";
import { Pagination } from "@material-ui/lab";
import { useSelector } from "react-redux";
import Vrtiljak from "../Locations/Vrtiljak";
import Waves from "../Animations/Waves";
import ContactUs from "../Subpages/ContactUs";
import MapContainerNew from "../Map/MapContainerNew";

const Home = ({ setCurrentId }) => {
  const classes = Styles();

  const locations = useSelector((state) => state.locations);

  return (
    <div>
      <Box className={classes.hero}>
        <Waves />
      </Box>

      <br />
      <br />
      <br />

      <Container maxWidth="lg" className={classes.blogsContainer}>
        <Typography variant="h5" className={classes.text1}>
          LOKACIJE PO CELI SLOVENIJI
        </Typography>

        <br />
        <br />

        <Typography variant="h6" className={classes.blogTitle}>
          <Box mt={2} ml={2}>
            {/* <MapContainer /> */}
            <MapContainerNew />
          </Box>
        </Typography>

        <br />
        <br />
        <br />
        <Grid container spacing={10} justifyContent="center">
          <Grid item xs={10} sm={6} md={4}>
            <Typography variant="h4" className={classes.blogTitle}>
              <Vrtiljak lokacije={locations} t={"max"} />
            </Typography>
          </Grid>
          <Grid item xs={10} sm={6} md={4}>
            <Typography variant="h4" className={classes.blogTitle}>
              <Vrtiljak lokacije={locations} t={"min"} />
            </Typography>
          </Grid>
        </Grid>

        <br />
        <br />
        <br />

        <Typography variant="h5" className={classes.text1}>
          BLOG
        </Typography>
        <br />
        <br />
        <Blog />
        <Box my={4} className={classes.paginationContainer}>
          <Pagination count={1} />
        </Box>

        <br />
        <br />

        <ContactUs />
      </Container>
    </div>
  );
};
export default Home;
