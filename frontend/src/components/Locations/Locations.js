import React from "react";
import Location from "./Location/Location";
import { CircularProgress, Card } from "@material-ui/core";
import { useSelector } from "react-redux";
import { Grid } from "@material-ui/core";

const Locations = ({ data, search, setCurrentId }) => {
  const locations = useSelector((state) => state.locations);
  let ocena;
  let regija;
  let voda;
  if (data[0] == undefined || data[0] == "") {
    ocena = "";
  } else {
    ocena = data[0][0];
  }
  if (data[1] == undefined || data[1] == "") {
    regija = "";
  } else {
    regija = data[1][0];
  }
  if (data[2] == undefined || data[2] == "") {
    voda = "";
  } else {
    voda = data[2][0];
  }

  let l = locations;
  l = main(voda, regija, ocena, locations);
  if (search == "") {
  } else {
    l = searchLocations(search, l);
  }

  function searchLocations(search, locations) {
    let loc = [];
    for (let i = 0; i < locations.length; i++) {
      if (
        locations[i].title.toUpperCase().includes(search.toUpperCase()) ||
        locations[i].desc.toUpperCase().includes(search.toUpperCase()) ||
        locations[i].tags[0].toUpperCase().includes(search.toUpperCase())
      ) {
        loc.push(locations[i]);
      }
    }
    return loc;
  }

  function main(selectedVoda, selectedRegija, selectedOcena, locations) {
    let filtriraneLokacije = locations;
    if (selectedVoda) {
      filtriraneLokacije = water(selectedVoda, filtriraneLokacije);
    }
    if (selectedRegija) {
      filtriraneLokacije = regions(selectedRegija, filtriraneLokacije);
    }
    if (selectedOcena) {
      filtriraneLokacije = popularity(selectedOcena, filtriraneLokacije);
    }
    return filtriraneLokacije;
  }

  if (
    l.length < 1 &&
    (data[0] != undefined || data[1] != undefined || data[2] != undefined)
  ) {
    alert(
      "Vaše iskanje ni bilo uspešno. \r\n Prilagodite iskalne parametre za večji uspeh \r\n ali dopolnite naš seznam."
    );
  }

  function water(selectedVoda, locations) {
    let loc = [];
    for (let i = 0; i < locations.length; i++) {
      if (
        locations[i].tags[0].toUpperCase().includes(selectedVoda.toUpperCase())
      ) {
        if (loc.indexOf(locations[i]) == -1) {
          loc.push(locations[i]);
        }
      }
    }
    return loc;
  }

  function regions(selectedRegija, locations) {
    let loc = [];
    for (let i = 0; i < locations.length; i++) {
      for (let j = 0; j < selectedRegija.length; j++) {
        if (
          locations[i].tags[0]
            .toUpperCase()
            .includes(selectedRegija[j].toUpperCase())
        ) {
          if (loc.indexOf(locations[i]) == -1) {
            loc.push(locations[i]);
          }
        }
      }
    }
    return loc;
  }

  function popularity(selectedOcena, locations) {
    let vsota = 0;
    for (let i = 0; i < locations.length; i++) {
      vsota += locations[i].likes.length;
    }
    let povprecje = vsota / locations.length;
    if (selectedOcena == "največ") {
      return locations.filter((location) => location.likes.length >= povprecje);
    } else if (selectedOcena == "najmanj") {
      return locations.filter((location) => location.likes.length <= povprecje);
    }
  }

  return !l.length ? (
    <div
      style={{
        alignItems: "center",
        display: "flex",
        justifyContent: "center",
        height: "100vh",
        width: "100vw",
      }}
    >
      <CircularProgress />
    </div>
  ) : (
    <div style={{ padding: "10%" }}>
      <Grid container spacing={5} justifyContent="center">
        {l.map((location) => (
          <Grid
            item
            key={location._id}
            style={{ display: "flex" }}
            xs={8}
            sm={4}
            md={3}
          >
            <Card
              sx={{ height: "50%", display: "flex", flexDirection: "column" }}
            >
              <Location location={location} setCurrentId={setCurrentId} />
            </Card>
          </Grid>
        ))}
      </Grid>
    </div>
  );
};
export default Locations;
