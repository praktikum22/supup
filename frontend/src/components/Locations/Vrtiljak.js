import React from "react";
import { Link } from "react-router-dom";
import { Carousel } from "react-responsive-carousel";
import styles from "react-responsive-carousel/lib/styles/carousel.min.css";

const Vrtiljak = ({ t, lokacije, setCurrentId }) => {
  let text = false;
  if (t == "max") {
    text = true;
  }

  let locations = popularity(t, lokacije);
  function popularity(selectedOcena, locations) {
    let vsota = 0;
    for (let i = 0; i < locations.length; i++) {
      vsota += locations[i].likes.length;
    }
    let povprecje = vsota / locations.length;
    if (selectedOcena == "max") {
      return locations.filter((location) => location.likes.length >= povprecje);
    } else if (selectedOcena == "min") {
      return locations.filter((location) => location.likes.length <= povprecje);
    }
  }

  return (
    <>
      {text ? "Najbolj priljubljene lokacije:" : "Manj priljubljene lokacije:"}
      <Carousel width="300px" showArrows={true} infiniteLoop={true}>
        {locations.map((location) => (
          <Link to={`/location/${location._id}`} state={location}>
            <div
              key={location._id}
              style={{
                height: "200px",
                backgroundSize: "400px 200px",
                width: "400px",
                color: "#fff",
                backgroundImage: `URL(${location.selectedFile})`,
              }}
            >
              <p
                style={{
                  textDecorationThickness: "1px",
                  color: "white",
                  left: "6px",
                  bottom: "125px",
                  position: "absolute",
                  textShadow: "1px 1px black",
                  textAlign: "center",
                  textDecoration: "none",
                }}
              >
                {location.title}
              </p>
            </div>
          </Link>
        ))}
      </Carousel>
    </>
  );
};
export default Vrtiljak;
