import React, { useRef } from "react";
import Locations from "./Locations"
import Button from '@mui/material/Button';
import { Container } from "@material-ui/core";
import Searchbar from "../Filter/Searchbar";
import Filter from "../Filter/Filter";
import SearchIcon from '@mui/icons-material/Search';

function LocationsContainer() {

  const [data, setData] = React.useState([]);
  const [search, setSearch] = React.useState([]);
  const filterRef = useRef();
  const searchRef = useRef();
  return (
    <>
    <br />
    <Container>
      <Filter ref={filterRef} />
      <br />
      <Button
        onClick={() => {
          setData(filterRef.current.poslji());
        }}
        variant="contained"
        style={{backgroundColor: "#12949B"}}
      >
        Filtriraj
      </Button>
      
      <br />
      <br />
      <Searchbar ref={searchRef} />
      <Button
        onClick={() => {
          setSearch(searchRef.current.poslji2());
        }}><SearchIcon style={{color: "#12949B"}}/></Button>
    </Container>
    <Locations data={data} search={search} />
    </>
  )
}

export default LocationsContainer;
