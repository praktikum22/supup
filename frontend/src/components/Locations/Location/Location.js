import * as React from "react";
import Styles from "../../../Styles/Styles";
import { useDispatch } from "react-redux";
import { useNavigate, useParams } from "react-router-dom";
import {
  Container,
  CardMedia,
  CardActions,
  Button,
  Typography,
} from "@mui/material";
import Heart from "../../LocationDetails/Heart";

const Location = ({ location, setCurrentId }) => {
  const classes = Styles();
  let navigate = useNavigate();
  const handleExpandClick = () => {
    let path = `/location/${location._id}`;
    navigate(path, { state: location });
  };

  return (
    <div>
      <Container sx={{ py: 3 }} maxWidth="md" onClick={handleExpandClick}>
        <div>
          <Button
            size="small"
            onClick={() => setCurrentId(location._id)}
          ></Button>
        </div>
        <CardMedia
          component="img"
          sx={{
            objectFit: "none" /* Do not scale the image */,
            objectPosition: "center" /* Center the image within the element */,
            height: "100%",
            width: "100%",
            backgroundSize: "cover",
          }}
          image={
            location.selectedFile ||
            "https://user-images.githubusercontent.com/194400/49531010-48dad180-f8b1-11e8-8d89-1e61320e1d82.png"
          }
        />
        <Typography gutterBottom variant="h5" component="h2">
          {location.title}
        </Typography>
        <Typography>{location.message}</Typography>
        <Typography>{location.tags.map((tag) => `#${tag} `)}</Typography>
      </Container>
      <CardActions>
        <Heart id={location._id} array={location.likes} change={true} />
      </CardActions>
    </div>
  );
};
export default Location;
