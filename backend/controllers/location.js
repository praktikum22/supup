import express from "express";
import mongoose from "mongoose";
import LocationMessage from "../models/locationMessage.js";
const router = express.Router();
export const getLocation = async (req, res) => {
  try {
    const locationMessages = await LocationMessage.find();

    //console.log(locationMessages);

    res.status(200).json(locationMessages);
  } catch (error) {
    res.status(404).json({ message: error.message });
  }
};

export const getLocationDetails = async (req, res) => {
  const { id } = req.params;

  try {
    const location = await LocationMessage.findById(id);

    res.status(200).json(location);
  } catch (error) {
    res.status(404).json({ message: error.message });
  }
};

export const deleteLocation = async (req, res) => {
  const { id } = req.params;
  if (!mongoose.Types.ObjectId.isValid(id))
    return res.status(404).send(`No location with id: ${id}`);

  await LocationMessage.findByIdAndRemove(id);
  console.log("delete!");

  res.json({ message: "Location deleted successfully" });
};

export const createLocation = async (req, res) => {
  const location = req.body;

  const newLocation = new LocationMessage(location);

  try {
    await newLocation.save();

    res.status(201).json(newLocation);
  } catch (error) {
    res.status(409).json({ message: error.message });
  }
};

export const likeLocation = async (req, res) => {
  const { id } = req.params;
  const { user } = req.body;
  const userid = String(user);

  if (!mongoose.Types.ObjectId.isValid(id))
    return res.status(404).send(`No location with id: ${id}`);

  const location = await LocationMessage.findById(id);

  const index = location.likes.findIndex((id) => id === userid);

  if (index === -1) {
    location.likes.push(userid);
  } else {
    location.likes = location.likes.filter((id) => id !== userid);
  }

  const updatedLocation = await LocationMessage.findByIdAndUpdate(
    id,
    location,
    {
      new: true,
    }
  );

  console.log("like!");
  console.log(userid);

  res.status(200).json(updatedLocation);
};
