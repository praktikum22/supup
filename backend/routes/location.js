import express from "express";

import {
  getLocation,
  createLocation,
  getLocationDetails,
  deleteLocation,
  likeLocation,
} from "../controllers/location.js";

const router = express.Router();

router.get("/", getLocation);
router.post("/", createLocation);
router.get("/:id", getLocationDetails);
router.delete("/:id", deleteLocation);
router.patch("/:id/likeLocation", likeLocation);

export default router;
