import mongoose from "mongoose";

const locationSchema = mongoose.Schema({
  id: Number,
  creator: String,
  title: String,
  locationx: Number,
  locationy: Number,
  desc: String,
  likes: { type: [String], default: [" "] },
  tags: [String],
  selectedFile: String,
});

const LocationMessage = mongoose.model("LocationMessage", locationSchema);

export default LocationMessage;
